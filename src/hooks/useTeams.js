import { useReducer } from "react";

const TEAMS = [
  {
    name: "Red",
    players: ["Robin", "Rey", "Roger", "Richard"],
    games: [
      {
        score: 10,
        city: "LA",
      },
      {
        score: 1,
        city: "NJ",
      },
      {
        score: 3,
        city: "NY",
      },
    ],
  },
  {
    name: "Blue",
    players: ["Bob", "Ben"],
    games: [
      {
        score: 6,
        city: "CA",
      },
      {
        score: 3,
        city: "LA",
      },
    ],
  },
  {
    name: "Yellow",
    players: ["Yesmin", "Yuliana", "Yosemite"],
    games: [
      {
        score: 2,
        city: "NY",
      },
      {
        score: 4,
        city: "LA",
      },
      {
        score: 7,
        city: "AK",
      },
    ],
  },
];

const useTeams = () => {
  const getTotalPoints = (object) => {
    return Object.values(object).reduce((accumulator, value) => {
      return accumulator + value.score;
    }, 0);
  };

  const getTeamsWithTotalPoints = () => {
    return TEAMS.map((team) => {
      const teamTotalPoints = getTotalPoints(team.games);
      return {
        ...team,
        totalPoints: teamTotalPoints,
      };
    });
  };

  const createInitialState = () => {
    const teamsWithTotalPoints = getTeamsWithTotalPoints();
    return teamsWithTotalPoints;
  };

  const reduceTeams = (state, action) => {
    const teamsWithTotalPoints = getTeamsWithTotalPoints();
    switch (action.type) {
      case "highest_to_lowest":
        return teamsWithTotalPoints.sort(
          (a, b) => b.totalPoints - a.totalPoints
        );
      case "lowest_to_highest":
        return teamsWithTotalPoints.sort(
          (a, b) => a.totalPoints - b.totalPoints
        );
      case "3_players_or_more":
        return teamsWithTotalPoints.filter((team) => team.players.length >= 3);
      case "reset":
        return createInitialState();
      default:
        return state;
    }
  };

  const [teams, dispatchTeams] = useReducer(
    reduceTeams,
    TEAMS,
    createInitialState
  );

  return { teams, dispatchTeams };
};

export default useTeams;
