export const EXAMPLE_IMAGE =
  "https://cdn.pixabay.com/photo/2023/03/30/15/09/spring-7887962__340.jpg";

export const IMAGES = [
  "https://cdn.pixabay.com/photo/2023/04/17/15/45/comma-7932755_1280.jpg",
  "https://cdn.pixabay.com/photo/2023/03/24/03/18/boy-7873231_640.jpg",
  "https://cdn.pixabay.com/photo/2023/03/30/15/09/spring-7887962__340.jpg",
];

export const PRODUCTS = [
  {
    name: "Orange",
    votes: 0,
  },
  {
    name: "Apple",
    votes: 0,
  },
];

export const NEXT_ITEMS_KEYS = ["ArrowLeft", "ArrowDown"];
export const PREVIOUS_ITEMS_KEYS = ["ArrowRight", "ArrowUp"];
