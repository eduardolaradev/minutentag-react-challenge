/**
 * You have a Grocery component, which receives a list of products, each one with name and votes.
 * - The app should render an unordered list, with a list item for each product.
 * - Products can be upvoted or downvoted.
 * By appropriately using React state and props, implement the upvote/downvote logic. Keep the state in the topmost component, while the Product component should accept props.
 *
 * For example, passing the following array as products prop to Grocery
 * [{ name: "Oranges", votes: 0 }, { name: "Bananas", votes: 0 }]
 * and clicking the '+' button next to the Oranges should result in HTML like:
 *
 *   <ul>
 *     <li>
 *       <span>Oranges - votes: 1</span>
 *       <button>+</button>
 *       <button>-</button>
 *     </li>
 *     <li>
 *       <span>Bananas - votes: 0</span>
 *       <button>+</button>
 *       <button>-</button>
 *     </li>
 *   </ul>
 */

import { useState } from "react";

const Product = ({ name, votes, onVote }) => {
  const handlePlus = () => {
    onVote(name, votes + 1);
  };

  const handleMinus = () => {
    onVote(name, votes - 1);
  };

  return (
    <li>
      <span>
        {name} - votes: {votes}
      </span>
      <button onClick={handlePlus}>+</button>
      <button onClick={handleMinus}>-</button>
    </li>
  );
};

export const Grocery = ({ products }) => {
  const [productList, setProductList] = useState(products);

  /*This code updates the votes property of all products with the same name.
   *It could be a problem, but there should not be two equal products as different elements in the list.
   *In the best scenario, the products should have a quantity property.
   */
  const handleVote = (name, newVotes) => {
    const updatedList = productList.map((product) =>
      product.name === name ? { ...product, votes: newVotes } : product
    );
    setProductList(updatedList);
  };

  return (
    <ul>
      {productList.map((product) => (
        <Product
          key={product.name}
          name={product.name}
          votes={product.votes}
          onVote={handleVote}
        />
      ))}
    </ul>
  );
};
