/**
 * Implement the ImageGallery component that accepts a `links`
 * prop and renders the gallery so that the first
 * item in the links prop is the src attribute of the first image in the gallery.

 * It should also implement the following logic:
 * - When the button is clicked, the image that is in the same div as the button should be removed from the gallery.
 */

import { useState } from "react";

function Image({ src, onRemove }) {
  return (
    <div className="imageContainer">
      <img src={src} alt="gallery item" className="image" />
      <button className="remove" onClick={() => onRemove(src)}>
        X
      </button>
    </div>
  );
}

export function ImageGallery({ links }) {
  const [gallery, setGallery] = useState(links);

  const handleRemove = (src) => {
    const newGallery = gallery.filter((image) => image !== src);
    setGallery(newGallery);
  };

  return (
    <div className="imageGallery">
      {gallery?.map((image, index) => (
        <Image key={index} src={image} onRemove={handleRemove} />
      ))}
    </div>
  );
}
