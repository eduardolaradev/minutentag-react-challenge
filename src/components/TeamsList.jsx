/**
 * Given a list of teams, we need to expose the teams in different ways:
 * - Criteria 1: depending on the score, order the list from highest to lowest.
 * - Criteria 2: depending on the score, order the list from lowest to highest.
 * - Criteria 3: depending on the player's quantity, show ONLY the teams that has more than 3 players.
 *
 * What you need to implement is:
 * - 3 buttons. Each of them need to execute a criteria.
 * - The list of teams should be updated dynamically depending on the selected filter.
 * - Each team item should display: Team Name / Player’s quantity / Total Score of each team.
 */

import useTeams from "../hooks/useTeams";

export function TeamsList() {
  const { teams, dispatchTeams } = useTeams();

  // Order teams by score (highest to lowest)
  const orderTeamByScoreHighestToLowest = () => {
    dispatchTeams({ type: "highest_to_lowest" });
  };

  // Order teams by score (lowest to highest)
  const orderTeamByScoreLowestToHighest = () => {
    dispatchTeams({ type: "lowest_to_highest" });
  };

  // Filtering teams that with at least 3 players
  const teamsWithMoreThanThreePlayers = () => {
    dispatchTeams({ type: "3_players_or_more" });
  };

  const resetList = () => {
    dispatchTeams({ type: "reset" });
  };

  return (
    <div>
      <button onClick={resetList}>Initial list</button>

      <button onClick={orderTeamByScoreHighestToLowest}>
        Highest to Lowest
      </button>
      <button onClick={orderTeamByScoreLowestToHighest}>
        Lowest to Highest
      </button>
      <button onClick={teamsWithMoreThanThreePlayers}>
        Teams with at least 3 players
      </button>

      <ul className="teams">
        {teams.map((team) => (
          <li key={team.name}>
            {team.name} / {team.players?.length} Players / {team.totalPoints}{" "}
            Points
          </li>
        ))}
      </ul>
    </div>
  );
}
