/**
 * Given a list of items implement a navigation through the keyboard, following these requirements:
 * - Navigate through the list with UP and RIGHT keys will focus the next item.
 * - Navigate through the list with DOWN and LEFT keys will focus the previous item.
 * - Only one item will be FOCUSED per time in the browser.
 *
 * Suggestion: you may to think in term of "indexes".
 * You may calculate the index and use it to select the item, then you will focus that item.
 *
 * NOTE: The keydown event will work once the <ul> receives the focus.
 */

import { useEffect, useRef, useState } from "react";
import {
  EXAMPLE_IMAGE,
  NEXT_ITEMS_KEYS,
  PREVIOUS_ITEMS_KEYS,
} from "../constants";

function Image() {
  return <img src={EXAMPLE_IMAGE} alt="" />;
}

// Simulating a list of items to render.
// This can be passed through props as well. The constant is declared here for convenience
const itemsList = Array(10).fill(<Image />);

// I didnt use the prop items for this example
export function ListItemsForNavigation({ items }) {
  const [selectedIndex, setSelectedIndex] = useState(null);
  const activeItemRef = useRef();

  useEffect(() => {
    if (activeItemRef?.current) {
      activeItemRef.current.focus();
    }
  }, [selectedIndex]);

  const handleKeyDown = (event) => {
    event.preventDefault();
    const { key } = event;
    let index;

    if (NEXT_ITEMS_KEYS.includes(key)) {
      index = selectedIndex === 0 ? itemsList.length - 1 : selectedIndex - 1;
    } else if (PREVIOUS_ITEMS_KEYS.includes(key)) {
      index = selectedIndex === itemsList.length - 1 ? 0 : selectedIndex + 1;
    }
    setSelectedIndex(index);
  };

  return (
    <ul onKeyDown={handleKeyDown} className="navigationList">
      {itemsList.map((item, index) => (
        <li
          key={index}
          tabIndex={0}
          ref={index === selectedIndex ? activeItemRef : null}
          className="navigationImage"
          onClick={() => setSelectedIndex(index)}
        >
          {item}
        </li>
      ))}
    </ul>
  );
}
