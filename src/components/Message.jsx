/*
 * The Message component contains an anchor element and
 * a paragraph below the anchor. Rendering of the paragraph
 * should be toggled by clicking on the anchor element
 * using the following logic:
 *
 * - At the start, the paragraph should not be rendered.
 * - After a click, the paragraph should be rendered.
 * - After another click, the paragraph should not be rendered.
 * - Finish the Message component by implementing this logic.
 */
import { useState } from "react";

export function Message() {
  //Declare a control state showPhone to decide when to render the paragraph
  const [showPhone, setShowPhone] = useState(false);

  //Set the value of showPhone control state when clicking the anchor
  const handleClick = (event) => {
    event.preventDefault();
    setShowPhone(!showPhone);
  };
  //Render the paragraph when showPhone is true
  return (
    <>
      <a href="#" onClick={handleClick}>
        Want to buy a new car?
      </a>
      {showPhone && <p>Call +11 22 33 44 now!</p>}
    </>
  );
}
